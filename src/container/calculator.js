import React, { useState } from "react";
import { StyleSheet, FlatList, View, Text, TouchableOpacity, Image } from "react-native";
import Display from "./display";
import globalStyles from "./styles";
import HorizontalScroll from "./horizontalScroll";

const Calculator = () => {
    const [answer, setAnswer] = useState('');
    const [string, setString] = useState('0');
    const [history, setHistory] = useState([]);
    const [count, setCount] = useState(1);
    const [viewHistory, setViewHistory] = useState(false);
    const operators = ['+', '-', '*', '/', '%'];
    const expressions = ['% = undefined', '/ = undefined', '* = undefined', '- = undefined', '+ = undefined', ' = ', ' = undefined'];

    const validateExpressions = (expression) => {
        var checkExpression = expression.split('=');
        var firstHalf = checkExpression[0];
        var booleanExpression = false;
        // if (!(operators.includes(firstHalf[firstHalf.length - 1]))) {
        for (let char of firstHalf) {
            if (operators.includes(char)) {
                booleanExpression = true;
            }
        }
        if (booleanExpression === true) {
            handleHistory(firstHalf + ' = ' + checkExpression[1]);
        }
        // }
    };

    const showResult = (finalResult) => {
        try {
            setAnswer(eval(finalResult));
        }
        catch {
            try {
                var result = eval(finalResult.slice(0, -1))
                if (result === undefined) setAnswer('')
                else setAnswer(result);
            }
            catch { }
        }
    };

    const showHistory = () => {
        setViewHistory(!viewHistory);
    }

    const handleHistory = (string) => {
        // if (!(expressions.includes(string))) {
            if (history.length === 10) {
                var newHistory = history;
                newHistory.pop();
                setHistory(newHistory);
            }
            else {
                setHistory((previousHistory) => {
                    return ([
                        { his: string, key: Math.random().toString() },
                        ...previousHistory
                    ])
                })
            }
        // }
    }

    const pressHandler = (value) => {

        if (value === '=') {
            if (string === '0') { }
            else {
                if (string.length !== 0) {
                    // handleHistory(string + '=' + answer);
                    validateExpressions(string + '=' + answer);
                }
                setAnswer(answer);
                setString('');
            }
        }
        else if (value === '\u232B') {
            setString(string.slice(0, -1));
            if (string.length === 0) {
                var stringEmpty = '0';
                setAnswer(stringEmpty)
            }
            if (string.length !== 0) {
                showResult(string.slice(0, -1))
            }
        }
        else if (value === 'AC') {
            setCount(count + 1);
            setString('0');
            setAnswer('');
            if (count === 2) {
                setHistory([]);
                setCount(1);
            }
        }
        else {
            if (operators.includes(string[string.length - 1]) && operators.includes(value)) {
                setString(string.slice(0, -1).concat(value));
            }
            else if ((string[string.length - 1] === '.') && (value === '.')) {

            }
            else if ((operators.includes(string[string.length - 1]) && value === '.') || ((string.length === 0) && (value === '.'))) {
                var temp = string.concat(value);
                var addZero = temp.concat(0);
                setString(temp);
                showResult(addZero);
            }
            else {
                if (!(string.length === 12)) {
                    if ((answer.length === 0 || answer.length !== 0) && (operators.includes(value))) {
                        var continutionResult;
                        if (string.length === 0) {
                            continutionResult = answer + value;
                            setString(continutionResult);
                            showResult(continutionResult);
                        }
                        else if (string.length !== 0) {
                            continutionResult = string + value;
                            setString(continutionResult);
                            showResult(continutionResult);
                        }
                    }
                    else {
                        var finalResult;
                        if (string === '0') finalResult = value
                        else finalResult = string + value;
                        setString(finalResult);
                        showResult(finalResult);
                    }
                }

            }
        }

    }

    const displayKeyboard = [
        { button: 'AC', key: '1' }, { button: '%', key: '2' }, { button: '\u232B', key: '3' }, { button: '/', key: '4' },
        { button: '7', key: '5' }, { button: '8', key: '6' }, { button: '9', key: '7' }, { button: '*', key: '8' },
        { button: '4', key: '9' }, { button: '5', key: '10' }, { button: '6', key: '11' }, { button: '-', key: '12' },
        { button: '1', key: '13' }, { button: '2', key: '14' }, { button: '3', key: '15' }, { button: '+', key: '16' },
        { button: '00', key: '17' }, { button: '0', key: '18' }, { button: '.', key: '19' }, { button: '=', key: '20' },
    ]

    return (
        <View style={styles.container}>
            <View style={styles.resultView}>
                <Text style={globalStyles.textField}>{string}</Text>
                <Text style={globalStyles.textField}>{answer}</Text>
            </View>
            <View style={styles.horizontalScrollView}>
                <TouchableOpacity onPress={() => showHistory()}>
                    <Image style={{ height: 30, width: 30, marginHorizontal: 5 }} source={require('../assets/history.png')} />
                </TouchableOpacity>
                {viewHistory && <FlatList
                    horizontal
                    data={history}
                    renderItem={({ item }) => {
                        return (
                            <HorizontalScroll item={item} />
                        )
                    }}
                />}
            </View>
            <View style={styles.buttonsView}>
                <FlatList
                    scrollEnabled={false}
                    data={displayKeyboard}
                    numColumns='4'
                    renderItem={({ item }) => {
                        return (
                            <Display item={item} pressHandler={pressHandler} />)
                    }
                    }
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'flex-end'
    },
    resultView: {
        // flex: 1.6,
        // justifyContent: 'flex-end',
        // backgroundColor: 'green'
    },
    buttonsView: {
        // flex: 3,
    },
    horizontalScrollView: {
        flex: 0.4,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'skyblue',
    }
})

export default Calculator;