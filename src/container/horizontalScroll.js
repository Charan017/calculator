import React from "react";
import { Text, View, StyleSheet } from "react-native";

const HorizontalScroll = ({ item }) => {
    return (
        <View style={styles.historyView}>
            <Text style={styles.historyText}>{item.his}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    historyView: {
        flex: 1,
        justifyContent: 'center'
    },
    historyText: { 
        fontSize: 30, 
        borderRadius: 5, 
        marginHorizontal: 10, 
        backgroundColor: 'aliceblue', 
        borderRadius: 20, 
        paddingHorizontal: 10
    }
})

export default HorizontalScroll;