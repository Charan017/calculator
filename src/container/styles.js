import { StyleSheet } from "react-native";

const globalStyles = StyleSheet.create({
    textField: {
        height: 60,
        textAlign:'right',
        color: 'black',
        fontWeight: "300",
        fontSize: 40,
    },
})

export default globalStyles;