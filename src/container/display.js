import React from "react";
import { View, Text,TouchableOpacity, StyleSheet } from "react-native";

const Display = ({item, pressHandler}) =>{
    
    return (
        <View style = {styles.container}>
                <TouchableOpacity style = {styles.pressable} onPress = {()=>pressHandler(item.button)}>
                    <Text style = {styles.text}>{item.button}</Text>
                </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eee',
        alignItems: 'center',
        height: 80
    },
    pressable: {
        width: 60,
    },
    text: {
        fontSize: 30,
        textAlign: 'center',
        margin:10
    }
})

export default Display;